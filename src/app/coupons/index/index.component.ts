import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-coupons',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    company;
    deleteModal: any;
    itemToDelete: any;
    coupons;
    rows: Array<any>;

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data['id'];
            this.company = await this.restangular.one('companies', this.company_id).get().toPromise();
            this.rows = await this.restangular.one('companies', this.company_id).all('coupons').getList().toPromise();
            this.coupons = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.coupons.filter(function(d) {
            return d.title && d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.one('companies', this.company_id).all('coupons').getList().toPromise();
        this.coupons = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}

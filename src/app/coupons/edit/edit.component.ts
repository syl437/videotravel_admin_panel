import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    public loading = false;
    coupon;
    coupon_id: number;
    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required),
        quantity: new FormControl('', Validators.required),
        price: new FormControl('', Validators.required),
        description_en: new FormControl('', Validators.required),
        description_he: new FormControl('', Validators.required),
    });
    image = {path: null, file: null};
    errors: Array<any> = [];
    company_id: number;

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.parent.params.subscribe(parentParams => {
            this.company_id = parentParams.id;

            this.activatedRoute.params.subscribe(async (params) => {

                this.coupon_id = params.id;
                console.log(this.company_id, this.coupon_id);
                this.coupon = await this.restangular.one('companies', this.company_id).one('coupons',  this.coupon_id).get().toPromise();
                this.form.setValue({
                    title_en: this.coupon.title_en,
                    title_he: this.coupon.title_he,
                    quantity: this.coupon.quantity,
                    price: this.coupon.price,
                    description_en: this.coupon.description_en,
                    description_he: this.coupon.description_he,
                });
                this.image.path = this.coupon.image;
            })
        });
    }

    onFileChange(event) {
        let file = event.target.files[0];
        let reader = new FileReader();
        reader.onload = ev => {
            this.image.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
            this.image.file =  file;
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        this.loading = true;
        this.coupon.title_en = this.form.value.title_en;
        this.coupon.title_he = this.form.value.title_he;
        this.coupon.quantity = this.form.value.quantity;
        this.coupon.price = this.form.value.price;
        this.coupon.description_en = this.form.value.description_en;
        this.coupon.description_he = this.form.value.description_he;

        let response = await this.coupon.patch().toPromise();    // TODO: process errors

        if (this.image.file){
            let fd = new FormData();
            fd.append('entity_id', this.coupon.id);
            fd.append('entity_type', 'coupon');
            fd.append('media_key', 'coupon');
            fd.append('coupon', this.image.file);

            await this.restangular.all('files').customPOST(fd).toPromise();
        }

        this.loading = false;
        this.router.navigate(['/companies/' + this.company_id + '/coupons']);
    }


}


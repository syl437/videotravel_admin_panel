import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {EditComponent} from './edit/edit.component';
import {CreateComponent} from './create/create.component';

export const CouponsRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Coupons'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'Create coupon'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Coupon'},
        }
    ]
}];

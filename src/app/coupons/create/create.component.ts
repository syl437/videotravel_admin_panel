import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    public loading = false;
    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required),
        quantity: new FormControl('', Validators.required),
        price: new FormControl('', Validators.required),
        description_en: new FormControl('', Validators.required),
        description_he: new FormControl('', Validators.required),
    });
    image = {path: null, file: null};
    errors: Array<any> = [];
    company_id: number;

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {this.company_id = data['id'];});
    }

    onFileChange(event) {
        let file = event.target.files[0];
        let reader = new FileReader();
        reader.onload = ev => {
            this.image.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
            this.image.file =  file;
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        this.loading = true;
        let coupon = this.restangular.restangularizeElement(this.restangular.one('companies', this.company_id), {}, 'coupons');
        coupon.title_en = this.form.value.title_en;
        coupon.title_he = this.form.value.title_he;
        coupon.quantity = this.form.value.quantity;
        coupon.price = this.form.value.price;
        coupon.description_en = this.form.value.description_en;
        coupon.description_he = this.form.value.description_he;

        let response = await coupon.save().toPromise();    // TODO: process errors

        let fd = new FormData();
        fd.append('entity_id', response.id);
        fd.append('entity_type', 'coupon');
        fd.append('media_key', 'coupon');
        fd.append('coupon', this.image.file);

        await this.restangular.all('files').customPOST(fd).toPromise();

        this.loading = false;
        this.router.navigate(['/companies/' + this.company_id + '/coupons']);
    }


}


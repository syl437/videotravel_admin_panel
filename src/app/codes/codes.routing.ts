import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';

export const CodesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Codes'},
        }
    ]
}];

import {Routes} from '@angular/router';
import {IndexComponent} from "./index/index.component";

export const MessagesRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: IndexComponent,
        data: {heading: 'הודעות'},
    }
    ]
}];

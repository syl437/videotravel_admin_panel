import {Routes} from '@angular/router';
import {IndexComponent} from "./index/index.component";

export const RegionsRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: IndexComponent,
        data: {heading: 'Regions'},
    }
    ]
}];

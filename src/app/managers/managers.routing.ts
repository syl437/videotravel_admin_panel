import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {EditComponent} from './edit/edit.component';
import {CreateComponent} from './create/create.component';
import {ShowComponent} from './show/show.component';

export const ManagersRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Managers'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'Create manager'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Manager'},
        },
        {
            path: ':id',
            component: ShowComponent,
            data: {heading: ''},
        }
    ]
}];

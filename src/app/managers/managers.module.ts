import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {IndexComponent} from "./index/index.component";
import { EditComponent } from './edit/edit.component';
import {CreateComponent} from "./create/create.component";

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { TreeModule } from 'angular-tree-component';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';
import {LoadingModule} from 'ngx-loading';
import {ManagersRoutes} from './managers.routing';
import {ShowComponent} from './show/show.component';

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild(ManagersRoutes),
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbProgressbarModule,
        CustomFormsModule,
        TreeModule,
        FileUploadModule,
        LoadingModule,
        NgbModule,
        TextMaskModule],
    declarations: [IndexComponent, CreateComponent, EditComponent, ShowComponent]
})

export class ManagersModule {}
import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-banners',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    itemToDelete: any;
    banners;
    rows: Array<any>;

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('banners').getList().toPromise();
        this.banners = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.banners.filter(function(d) {
            // return d.title && d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('banners').getList().toPromise();
        this.banners = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}

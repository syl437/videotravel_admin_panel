import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    public loading = false;
    image = {path: null, file: null};
    errors: Array<any> = [];
    company_id: number = 0;
    language: string;
    companies;
    banner;
    explanation: boolean = false;

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.banner = await this.restangular.one('banners', data.id).get().toPromise();
            this.companies = await this.restangular.all('companies').getList().toPromise();
            this.explanation = this.banner.type === 'explanation';
            this.company_id = this.banner.company_id;
            this.image.path = this.banner.image;
            this.language = this.banner.language;
        })
    }

    onFileChange(event) {
        let file = event.target.files[0];
        let reader = new FileReader();
        reader.onload = ev => {
            this.image.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
            this.image.file =  file;
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (!this.explanation && this.company_id === 0){
            this.errors.push({message: 'Please choose company or change the banner type'});
            return;
        }

        if (this.language === ''){
            this.errors.push({message: 'Please choose language'});
            return;
        }

        this.loading = true;

        this.banner.type = this.explanation ? 'explanation' : 'ad';
        this.banner.company_id = this.explanation ? null : this.company_id;
        this.banner.language = this.language;
        await this.banner.patch().toPromise();

        if (this.image.file){
            let fd = new FormData();
            fd.append('entity_id', this.banner.id);
            fd.append('entity_type', 'banner');
            fd.append('media_key', 'image');
            fd.append('image', this.image.file);

            await this.restangular.all('files').customPOST(fd).toPromise();
        }

        this.loading = false;
        this.router.navigate(['/banners']);
    }


}

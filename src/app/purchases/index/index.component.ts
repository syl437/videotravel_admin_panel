import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-purchases',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    rows;
    purchases = [];

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (params) => {
            this.company_id = params.id;
            this.purchases = await this.restangular.one('companies', this.company_id).all('purchases').getList().toPromise();
            this.rows = this.purchases;
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.purchases.filter(function(d) {
            return d.coupon.title && d.coupon.title.toLowerCase().indexOf(val) !== -1 ||
                    d.user.email && d.user.email.toLowerCase().indexOf(val) !== -1 ||
                    d.user.name && d.user.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

}

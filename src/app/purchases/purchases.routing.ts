import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';

export const PurchasesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Purchases'},
        }
    ]
}];

import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('id')) {
            switch(localStorage.getItem('type')){
                case 'admin':
                    return true;
                case 'manager':
                    if (state.url !== '/companies/' + localStorage.getItem('company_id') + '/managers/' + localStorage.getItem('id')){
                        this.router.navigate(['/companies/' + localStorage.getItem('company_id') + '/managers/' + localStorage.getItem('id')]);
                        return true;
                    }
                    return true;
                default:
                    return true;
            }
            // return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/authentication']);
        return false;
    }
}
import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-videos',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    company;
    deleteModal: any;
    itemToDelete: any;
    rows;
    errors = [];

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data['id'];
            this.company = await this.restangular.all('files').customGET('', {company_id: this.company_id}).toPromise();
            this.rows = this.company.medias;
        });
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.company = await this.restangular.all('files').customGET('', {company_id: this.company_id}).toPromise();
        this.rows = this.company.medias;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = this.restangular.restangularizeElement('', item, 'files');
    }

    async changeStatus (item, status) {
        await this.restangular.one('files', item.id).customPOST({status: status, user_id: item.user.id}, 'status').toPromise();
        this.company = await this.restangular.all('files').customGET('', {company_id: this.company_id}).toPromise();
        this.rows = this.company.medias;
    }

}

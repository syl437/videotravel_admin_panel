import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';

export const VideosRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Videos'},
        }
    ]
}];

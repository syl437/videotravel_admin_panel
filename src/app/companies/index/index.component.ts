import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
    selector: 'app-companies',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    detailsModal: any;
    companyToDelete: any;
    selectedCompany: any;
    companies;
    rows: Array<any>;

    constructor(public restangular: Restangular, private modalService: NgbModal, public router: Router) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('companies').getList().toPromise();
        this.companies = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.companies.filter(function(d) {
            return d.title && d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteCompany(){
        await this.companyToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('companies').getList().toPromise();
        this.companies = this.rows;
    }

    openDeleteModal(content, company) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = company;
    }

    openDetailsModal(content, company){
        this.detailsModal = this.modalService.open(content);
        this.selectedCompany = company;
    }

    goToDetails (page) {
        this.detailsModal.close();
        this.router.navigate(['/companies/' + this.selectedCompany.id + "/" + page]);
    }

    async approveCompany (company) {
        await company.customGET('approve').toPromise();
        this.rows = await this.restangular.all('companies').getList().toPromise();
        this.companies = this.rows;
    }

    async suspendCompany (company) {
        await company.customGET('suspend').toPromise();
        this.rows = await this.restangular.all('companies').getList().toPromise();
        this.companies = this.rows;
    }
}

import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {EditComponent} from './edit/edit.component';
import {CreateComponent} from './create/create.component';

export const CompaniesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Companies'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'Create company'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Company'},
        }
    ]
}];

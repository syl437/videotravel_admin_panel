import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {Router} from '@angular/router';
import * as Quill from 'quill';


@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    public loading = false;
    form: FormGroup = this.fb.group({
        question_en: new FormControl('', Validators.required),
        question_he: new FormControl('', Validators.required),
        answer_en: new FormControl('', Validators.required),
        answer_he: new FormControl('', Validators.required),
    });
    errors: Array<any> = [];

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router) {}

    ngOnInit() {
        const quill_en = new Quill('#editor-container-en', {
            modules: {toolbar: {container: '#toolbar-toolbar-en'}},
            theme: 'snow'
        });

        const quill_he = new Quill('#editor-container-he', {
            modules: {toolbar: {container: '#toolbar-toolbar-he'}},
            theme: 'snow'
        });

        quill_en.on('text-change', () => {
            this.form.controls.answer_en.setValue(quill_en.root.innerHTML);
        });

        quill_he.on('text-change', () => {
            this.form.controls.answer_he.setValue(quill_he.root.innerHTML);
        });
    }

    async onSubmit() {

        let faq = this.restangular.restangularizeElement('', {}, 'faq');
        faq.question_en = this.form.value.question_en;
        faq.question_he = this.form.value.question_he;
        faq.answer_en = this.form.value.answer_en;
        faq.answer_he = this.form.value.answer_he;

        await faq.save().toPromise();
        this.router.navigate(['/faq']);

    }


}

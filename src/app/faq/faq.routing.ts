import {Routes} from '@angular/router';
import {IndexComponent} from "./index/index.component";
import {EditComponent} from "./edit/edit.component";
import {CreateComponent} from "./create/create.component";

export const FaqRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: IndexComponent,
        data: {heading: 'FAQ'},
    },
    {
        path: 'create',
        component: CreateComponent,
        data: {heading: 'Create article'},
    },
    {
        path: 'edit/:id',
        component: EditComponent,
        data: {heading: 'Article'},
    }
    ]
}];

import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-faq',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    itemToDelete: any;
    faq;
    rows: Array<any>;

    constructor(public restangular: Restangular, private modalService: NgbModal) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('faq').getList().toPromise();
        this.faq = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.faq.filter(function(d) {
            return d.title && d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('faq').getList().toPromise();
        this.faq = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}

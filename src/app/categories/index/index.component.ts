import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-categories',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    id: number;
    subs: any;
    deleteModal: any;
    createModal: any;
    editMode: boolean = false;
    editRow: any = null;
    categoryToDelete: any;
    fieldsToEdit = {title_en: '', title_he: ''};
    rows: Array<any>;
    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required)
    });

    constructor(public restangular: Restangular,
                private modalService: NgbModal,
                public fb: FormBuilder,
                private activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.id = data['id'];
            this.subs = await this.restangular.one('company_categories', this.id).get().toPromise();
            this.rows = this.subs;
        });

    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.subs.filter(function(d) {
            return d.title_en && d.title_en.toLowerCase().indexOf(val) !== -1 || d.title_he && d.title_he.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    switchToEditMode (item) {
        this.editMode = true;
        this.editRow = this.restangular.restangularizeElement('', item, 'company_subcategories');
        console.log(item);
        this.fieldsToEdit.title_en = item.title_en;
        this.fieldsToEdit.title_he = item.title_he;
    }

    async editItem () {
        this.editRow.title_en = this.fieldsToEdit.title_en;
        this.editRow.title_he = this.fieldsToEdit.title_he;
        await this.editRow.patch().toPromise();
        this.subs = await this.restangular.one('company_categories', this.id).get().toPromise();
        this.rows = this.subs;
        this.cancelEdit();
    }

    cancelEdit () {
        this.editMode = false;
        this.editRow = null;
    }

    async deleteItem(){
        await this.categoryToDelete.remove().toPromise();
        this.deleteModal.close();
        this.subs = await this.restangular.one('company_categories', this.id).get().toPromise();
        this.rows = this.subs;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.categoryToDelete = this.restangular.restangularizeElement('', item, 'company_subcategories');
    }

    openCreateModal (content) {
        this.createModal = this.modalService.open(content);
    }

    async createItem (){
        let sub = this.restangular.restangularizeElement('', {
            title_en: this.form.value.title_en,
            title_he: this.form.value.title_he,
            company_category_id: this.id
        }, 'company_subcategories');
        await sub.save().toPromise();
        this.createModal.close();
        this.subs = await this.restangular.one('company_categories', this.id).get().toPromise();
        this.rows = this.subs;
    }

}

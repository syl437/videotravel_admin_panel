import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class IndexComponent implements OnInit {

    saved: boolean = false;
    form: FormGroup = this.fb.group({
        phone: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required)
    });

    constructor(public restangular: Restangular, public fb: FormBuilder) {}

    async ngOnInit() {
        let admin = await this.restangular.one('admins', 1).get().toPromise();
        this.form.setValue({phone: admin.phone, email: admin.email});
    }

    async onSubmit () {
        await this.restangular.one('admins', 1).customPATCH({email: this.form.value.email, phone: this.form.value.phone}).toPromise();
        this.saved = true;
    }

}
